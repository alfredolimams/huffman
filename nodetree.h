#ifndef NODETREE_H
#define NODETREE_H
#include <QString>
class NodeTree
{
    uchar m_c;
    unsigned int m_freq;
    NodeTree * m_left;
    NodeTree * m_right;
    bool m_leaf;
public:
    NodeTree(unsigned int freq, uchar c);
    unsigned int getFreq() const;
    void setFreq(unsigned int value);
    uchar getC() const;
    void setC(uchar value);
    NodeTree *getRight() const;
    void setRight(NodeTree *value);
    NodeTree *getLeft() const;
    void setLeft(NodeTree *value);
    bool isLeaf() const;
    void setLeaf(bool value);
    //QString toString();
    void set(NodeTree*left,NodeTree*right);
};

#endif // NODETREE_H
