#include "treehuffman.h"
#include <nodetree.h>
#include <Huffman.h>
#include <QList>
#include <QDebug>
#include <QtAlgorithms>
TreeHuffman::TreeHuffman(QList<NodeTree*> list)
{
    m_sizetree = 0;
    m_represent = "";
    m_binary = "";
    m_cod = new QString [256];
    qSort( list.begin() , list.end() , Huffman::compSort );
    while( list.size() > 2 )
    {
        NodeTree* aux = new NodeTree(list.at(0)->getFreq()+list.at(1)->getFreq(), 0);
        aux->set(list.at(0), list.at(1));
        list.removeFirst();
        list.removeFirst();
        list.insert(0, aux);
        if( list.at(0)->getFreq() >= list.at(1)->getFreq())
        {
            qSort(list.begin(), list.end(), Huffman::compSort);
        }
    }
    if( list.size() == 2 )
    {
        m_root = new NodeTree(list.at(0)->getFreq()+list.at(1)->getFreq(), 0);
        m_root->set(list.at(0), list.at(1));
        qDebug() << "Funfou!!!";
        buildRep_Cod( m_root , "");
    }
    else
    {
        qDebug() << "Puf!!!";
    }
}

NodeTree *TreeHuffman::root()
{
    return m_root;
}

void TreeHuffman::buildRep_Cod(NodeTree *no, QString str )
{
    if(no->isLeaf())
    {
        m_cod[no->getC()] = str;
        if(no->getC() == '*')
        {
            m_represent += "!*";
            m_sizetree++;
        }
        else if(no->getC() == '!')
        {
            m_represent += "!!";
            m_sizetree++;
        }
        else
        {
            m_represent += no->getC();
            m_sizetree++;
        }
    }
    else
    {
        m_represent += "*";
        m_sizetree++;
        str += '0';
        buildRep_Cod(no->getLeft(), str);
        str.chop(1);
        str += '1';
        buildRep_Cod(no->getRight(), str);
        m_sizetree++;
    }
}

QString TreeHuffman::getRep()
{
    return m_represent;
}

QString *TreeHuffman::getCode()
{
    return m_cod;
}

QString TreeHuffman::getBinary()
{
    return m_binary;
}

void TreeHuffman::ShowBinary()
{
    qDebug() << "-------------------------------------" << " Binary " << "---------------------------------";
    qDebug() << "Tamanho : " << m_binary.size();
    qDebug() << m_binary;
    qDebug() << "-------------------------------------" << " FIM " << "------------------------------------";

}

void TreeHuffman::binary(QString dir)
{
    QFile *file = new QFile(dir);
    if(file->open(QIODevice::ReadOnly))
    {
        QByteArray theFile = file->readAll();
        for (int var = 0 ; var < theFile.size() ; ++var)
            m_binary += m_cod[uchar(theFile.at(var))];
    }
}

void TreeHuffman::ShowTree(NodeTree* root, int level)
{
    if(!level)qDebug() << "-------------------------------" << " Tree Huffman " << "---------------------------------";
    if(root)
    {
        ShowTree(root->getRight(),level+1);
        qDebug() << qPrintable(QString("\t").repeated(level)) << uchar(root->getC()) << "--" << root->getFreq();
        ShowTree(root->getLeft(),level+1);
    }
    if(!level)qDebug() << "---------------------------------------" << "FIM" << "------------------------------------";
}

void TreeHuffman::ShowCod(QString *code)
{
    qDebug() << "----------------------------------" << "Codificação" << "---------------------------------";
    for( int i = 0 ; i < 256 ; ++i )
        if( code[i] != "" )
            qDebug() << uchar(i) << "---" << code[i];
    qDebug() << "---------------------------------------" << "FIM" << "------------------------------------";
}
void TreeHuffman::ShowList(QList<NodeTree*> list)
{
    qDebug() << "-----------------------------" << " Lista de frequência " << "---------------------------";
    for(int i = 0 ; i < list.size() ; ++i )
        qDebug() << list.at(i)->getFreq() << list.at(i)->getC();
    qDebug() << "-----------------------------------" << " FIM " << "--------------------------------------";
}
