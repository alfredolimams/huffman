#ifndef HUFFMAN_H
#define HUFFMAN_H
#include <QString>
#include <QDebug>
#include <QFile>
#include <QByteArray>
#include <QBitArray>
#include <QIODevice>
#include "nodetree.h"
#include "map.h"
// classe para leitura do arquivo e contagem da frequência de bytes
class Huffman
{
    int *freq;
    int m_lixo;
    QByteArray Hex;
public:
    Huffman(QString dir);
    ~Huffman();
    QList<NodeTree*> Queue(int *cont);
    QByteArray getHex();
    int * getFreq();
    static bool compSort(const NodeTree* a,const NodeTree* b);
    void StringBinarytoHex( QString binary );
    void countFreq(QString dir);
    void ShowHex();
};

#endif // HUFFMAN_H
