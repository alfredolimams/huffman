#include "Huffman.h"
Huffman::Huffman(QString dir)
{
    freq = new int [256];
    countFreq(dir);
}

Huffman::~Huffman()
{

}
void Huffman::countFreq(QString dir)
{
    QFile *file = new QFile(dir);
    if(file->open(QIODevice::ReadOnly))
    {
        QByteArray theFile = file->readAll();
        for( int var = 0 ; var < theFile.size() ; ++var)  freq[uchar(theFile.at(var))]++;
    }
    else freq = NULL;
}

void Huffman::ShowHex()
{
    //qDebug() << QByteArray::number(Hex.data(),16);
}
QList<NodeTree*>Huffman::Queue(int *cont)
{
    QList<NodeTree*> List;
    for( int i = 0 ; i < 256 ; ++ i )
        if(cont[i])
        {
            NodeTree * no = new NodeTree(cont[i], i);
            List.append(no);
        }
    return List;
}

QByteArray Huffman::getHex()
{
    return Hex;
}

bool Huffman::compSort(const NodeTree* a,const NodeTree* b){
    if( a->getFreq() == b->getFreq())
        return a->getC() < b->getC();
    return a->getFreq() < b->getFreq();
}

void Huffman::StringBinarytoHex(QString binary)
{
    Map *map = new Map();
    m_lixo = (8 - binary.size()%8)%8;
    binary += QString("0").repeated(m_lixo);
    Hex.resize(binary.size()/8);
    for( int i = 0, j = 0; i < binary.size() ; i += 8 )
    {
        QString str = "", hex = "";
        for(j = i; j < 4 + i ; ++j ) str += binary.at(j);
        hex += map->getSHex(str);
        str = "";
        for(; j < 8 + i ; ++j ) str += binary.at(j);
        hex += map->getSHex(str);
        Hex[i/8] = map->getHex(hex);
    }
}

int *Huffman::getFreq()
{
    return freq;
}
