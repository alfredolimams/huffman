#include <QDebug>
#include <QString>
#include "Huffman.h"
#include "treehuffman.h"
#include "nodetree.h"
#include <QTime>
#include <string.h>
#include <QByteArray>
#include "map.h"
int main()
{
    QTime time;
    time.start();
    QString diretorio = "/home/alfredo/Área de Trabalho/teste";
    Huffman *huff = new Huffman(diretorio);
    QList<NodeTree*> list;
    list = huff->Queue(huff->getFreq());
    TreeHuffman * tree = new TreeHuffman(list);
    tree->ShowList(list);
    tree->ShowTree(tree->root(),0);
    tree->ShowCod(tree->getCode());
    tree->binary(diretorio);
    tree->ShowBinary();
    huff->StringBinarytoHex(tree->getBinary());
    huff->ShowHex();
    qDebug("Tempo %d ms", time.elapsed());
}
