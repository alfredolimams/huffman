#include "nodetree.h"

NodeTree::NodeTree(unsigned int freq, uchar c)
{
    m_freq = freq;
    m_c = c;
    m_left = 0;
    m_right = 0;
}
bool NodeTree::isLeaf() const{
    return !m_leaf && !m_right;
}

void NodeTree::setLeaf(bool value)
{
    m_leaf = value;
}

void NodeTree::set(NodeTree *left, NodeTree *right)
{
    m_right = right;
    m_left = left;
}
unsigned int NodeTree::getFreq() const
{
    return m_freq;
}

void NodeTree::setFreq(unsigned int value)
{
    m_freq = value;
}

uchar NodeTree::getC() const
{
    return m_c;
}

void NodeTree::setC(uchar value)
{
    m_c = value;
}

NodeTree *NodeTree::getRight() const
{
    return m_right;
}

void NodeTree::setRight(NodeTree *value)
{
    m_right = value;
}

NodeTree *NodeTree::getLeft() const
{
    return m_left;
}

void NodeTree::setLeft(NodeTree *value)
{
    m_left = value;
}
