#include "map.h"

Map::Map()
{
    map = new QMap <QString,QString >();
    byte = new QMap <QString, int>();
    map->insert("0000","0");
    map->insert("0001","1");
    map->insert("0010","2");
    map->insert("0011","3");
    map->insert("0100","4");
    map->insert("0101","5");
    map->insert("0110","6");
    map->insert("0111","7");
    map->insert("1000","8");
    map->insert("1001","9");
    map->insert("1010","A");
    map->insert("1011","B");
    map->insert("1100","C");
    map->insert("1101","D");
    map->insert("1110","E");
    map->insert("1111","F");
    byte->insert("0",0);
    byte->insert("1",1);
    byte->insert("2",2);
    byte->insert("3",3);
    byte->insert("4",4);
    byte->insert("5",5);
    byte->insert("6",6);
    byte->insert("7",7);
    byte->insert("8",8);
    byte->insert("9",9);
    byte->insert("A",10);
    byte->insert("B",11);
    byte->insert("C",12);
    byte->insert("D",13);
    byte->insert("E",14);
    byte->insert("F",15);
}

int Map::getHex(QString sHex)
{
    return byte->value(sHex.at(0))*16 + byte->value(sHex.at(1));
}

QString Map::getSHex(QString bin)
{
    return map->value(bin);
}

