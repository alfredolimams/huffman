#ifndef MAP_H
#define MAP_H
#include <QMap>
#include <QString>
class Map
{
    QMap <QString,int > *byte;
    QMap <QString,QString > *map;
public:
    Map();
    QString getSHex( QString bin );
    int getHex(QString sHex );
};

#endif // MAP_H
