#ifndef TREEHUFFMAN_H
#define TREEHUFFMAN_H
#include <nodetree.h>
#include <Huffman.h>
#include <QList>
#include <QDebug>
#include <QtAlgorithms>
class TreeHuffman
{
        NodeTree * m_root;
        QList<NodeTree *> m_list;
        int m_sizetree;
        QString m_represent;
        QString m_binary;
        QString *m_cod;
public:  
    TreeHuffman(QList<NodeTree*> list);
    NodeTree * root();
    QString getRep();
    QString *getCode();
    QString getBinary();
    void ShowBinary();
    void buildRep_Cod(NodeTree *no, QString str );
    void code(NodeTree *node, QString temp);
    void binary(QString dir);
    void ShowList(QList<NodeTree *> list);
    void ShowTree(NodeTree* root ,int level = 0);
    void ShowCod(QString * code);
};
#endif // TREEHUFFMAN_H
