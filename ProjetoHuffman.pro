#-------------------------------------------------
#
# Project created by QtCreator 2015-05-06T14:01:22
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Projeto_Huffman
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Huffman.cpp \
    nodetree.cpp \
    treehuffman.cpp

HEADERS += \
    Huffman.h \
    nodetree.h \
    treehuffman.h
